﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot
{
    interface IMenu
    {
        void ShowMenu();
        void Action();
        void Run();
    }
}
