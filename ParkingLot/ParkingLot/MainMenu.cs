﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ParkingLot
{
    class MainMenu: IMenu
    {
        private readonly string menu = "\nPlease, choose action which you want to run...\n1. Current parking balance\n2. Cash earned by parking during last minute\n3. Show parking places(all - busy - free)\n4. Show vehicle info\n5. Add vehicle\n6. Remove vehicle\n7. Top up vehicle`s balance\n8. Show last minute transactions\n9. Show .log file\n10. Go to settings menu\n11. Show all vehicles\n0. Exit\n";
        public void ShowMenu()
        {
            Console.WriteLine(menu);
        }
        public void Action()
        {
            ushort choice;
            ShowMenu();
            bool valid = UInt16.TryParse(Console.ReadLine(), out choice);
            Console.Clear();
            if (!valid)
            {
                throw new UnknownCommandException("Please input a valid number...");
            }
            switch (choice)
            {
                case 1:
                    ShowParkingBalance();
                    break;
                case 2:
                    ShowLastMinuteParkingBalance();
                    break;
                case 3:
                    ShowAllParkingPlaces();
                    ShowFreeParkingPlaces();
                    ShowBusyParkingPlaces();
                    break;
                case 4:
                    ShowInfo();
                    break;
                case 5:
                    AddVehicle();
                    break;
                case 6:
                    RemoveVehicle();
                    break;
                case 7:
                    TopUpVehicleBalance();
                    break;
                case 8:
                    ShowLastMinuteTransactions();
                    break;
                case 9:
                    ReadLog();
                    break;
                case 10:
                    IMenu configuration = new SettingsMenu();
                    configuration.Run();
                    break;
                case 11:
                    ShowAllvehicles();
                    break;
                case 0:
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                    break;
                default:
                    Console.WriteLine("Please input valid number...");
                    break;
            }
            Timeout();

        }
        private void Timeout()
        {
            Console.ReadKey();
            Console.Clear();
        }
        public void Run()
        {
            while (true)
            {
                try
                {
                    Action();
                }
                catch (UnknownCommandException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (ParkingFullException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
        private void ReadLog()
        {
            string log = String.Empty;

            try
            {
                log = File.ReadAllText(Settings.LoggingFilePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Seems somethins goes wrong: " + e.Message);
                return;
            }

            Console.WriteLine("Transactions.log");
            Console.WriteLine(log);
        }

        private void ShowLastMinuteTransactions()
        {
            foreach (var transaction in Parking.Source.TransactionContainerForLastMinute)
            {
                Console.WriteLine(transaction.ToString());
            }
        }

        private void ShowParkingBalance()
        {
            Console.WriteLine($"Parking balance now: {Parking.Source.Balance}");
        }

        private void ShowLastMinuteParkingBalance()
        {
            Console.WriteLine($"Last minute parking balance: {Parking.Source.BalanceForLastMinute}");
        }

        private void ShowAllParkingPlaces()
        {
            Console.WriteLine($"This parking has {Parking.Source.ParkingPlaces} parking places");
        }

        private void ShowFreeParkingPlaces()
        {
            Console.WriteLine($"This parking has {Parking.Source.FreeParkingPlaces} free parking places");
        }

        private void ShowBusyParkingPlaces()
        {
            Console.WriteLine($"This parking has {Parking.Source.ParkingPlaces - Parking.Source.FreeParkingPlaces} busy parking places");
        }

        private void ShowInfo()
        {
            Console.WriteLine("Input individual serial number of vehicle(id): ");
            try
            {
                var Id = Guid.Parse(Console.ReadLine());
                Console.WriteLine(Parking.Source.ShowDetails(Id));
            }
            catch (FormatException)
            {
                Console.WriteLine("Please, input a valid individual VehicleId number...");
            }

        }
        private void AddVehicle()
        {
            Console.WriteLine("\nAvailable vehicle type:");
            foreach (var vType in Enum.GetValues(typeof(VehicleType)))
            {
                Console.WriteLine($" {(int)vType}. {vType}");
            }

            int type = 0;
            Console.Write("Enter type(number): ");
            bool valid = Int32.TryParse(Console.ReadLine(), out type);
            if (!valid || !Enum.IsDefined(typeof(VehicleType), type))
            {
                Console.WriteLine("Please, input number between 0 and 3...");
                return;
            }
            

            decimal value = 0;
            Console.Write("Enter amount of starter money: ");
            Decimal.TryParse(Console.ReadLine(), out value);

            var vehicle = new Vehicle((VehicleType)type, value);

            try
            {
                Parking.Source.AddVehicle(vehicle);
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine("Error");
                Console.WriteLine(ex.Message);
                return;
            }
            

            Console.WriteLine($"Success, your vehicle ID: {vehicle.Id}");
        }
        private void RemoveVehicle()
        {
            Console.Write("Enter your VehicleID: ");

            try
            {
                var id = Guid.Parse(Console.ReadLine());
                Parking.Source.RemoveVehicle(id);
            }
            catch (RemoveVehicleWithFineException e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            catch (FormatException)
            {
                Console.WriteLine("Please, input a valid individual VehicleId number..."); 
                return;
            }
            catch (InvalidOperationException e)
            {
                throw new UnknownCommandException("Please input valid number...");
                
            }


            Console.WriteLine($"Success");
        }
        private void ShowAllvehicles()
        {
            Console.WriteLine(Parking.Source.ShowAllVehicles());
        }
        private void TopUpVehicleBalance()
        {
            var Id = new Guid();
            decimal value = 0;
            decimal balance;
            Console.Write("Enter your VehicleID: ");
            Guid.TryParse(Console.ReadLine(), out Id);
            Console.Write("Enter amount: ");
            Decimal.TryParse(Console.ReadLine(), out value);

            try
            {
                balance = Parking.Source.TopUpVehicleBalance(Id, value);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("Something goes wrong: " + e.Message);
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine("Something goes wrong: " + e.Message);
                return;
            }

            Console.WriteLine($"Success, your balance now: {balance}");
        }
    }
}
