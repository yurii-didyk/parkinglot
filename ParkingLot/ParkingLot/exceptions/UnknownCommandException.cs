﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot
{
    class UnknownCommandException: Exception
    {
        public UnknownCommandException()
        {
        }

        public UnknownCommandException(string message)
            : base(message)
        {
        }

        public UnknownCommandException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
