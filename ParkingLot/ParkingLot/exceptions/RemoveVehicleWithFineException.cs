﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot
{
    class RemoveVehicleWithFineException: Exception
    {
        public RemoveVehicleWithFineException()
        {
        }

        public RemoveVehicleWithFineException(string message)
            : base(message)
        {
        }

        public RemoveVehicleWithFineException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
