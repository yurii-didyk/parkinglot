﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot
{
    class Vehicle
    {
        public Guid Id { get; }
        public decimal Balance { get; private set; }
        public VehicleType Type { get; }
        public Vehicle(VehicleType Type, decimal Balance = 0)
        {
            Id = Guid.NewGuid();
            this.Balance = Balance;
            this.Type = Type;
        }
        public void IncreaseBalance(decimal amount)
        {
            Balance += amount;
        }
        public void DecreaseBalance(decimal amount)
        {
            Balance -= amount;
        }
        public override string ToString()
        {
            return $"ID: {Id}, Vehicle Type: {Type.ToString()}, Balance: {Balance}";
        }

    }
}
