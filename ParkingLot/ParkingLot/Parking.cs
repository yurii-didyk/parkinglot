﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Linq;
using System.IO;
using System.Threading.Tasks;


namespace ParkingLot
{
    class Parking
    {
        
        private static readonly Lazy<Parking> LazyParking = new Lazy<Parking>(() => new Parking()); // For Singleton Pattern
        private static List<Vehicle> vehicles = new List<Vehicle>();
        private static List<Transaction> transactions = new List<Transaction>();
        public static Parking Source { get { return LazyParking.Value; } }
        public uint ParkingPlaces { get; set; }
        public uint FreeParkingPlaces
        {
            get
            {
                return (uint)(ParkingPlaces - vehicles.Count);
            }
        }
        public  decimal BalanceForLastMinute
        {
            get
            {
                return transactions.Where(transaction => transaction.Time >= DateTime.Now.AddMinutes(-1))
                    .Sum(transactions => transactions.Withdraw);
            }
        }
        public IEnumerable<Transaction> TransactionContainerForLastMinute
        {
            get
            {
                return transactions.Where(transaction => transaction.Time >= DateTime.Now.AddMinutes(-1));
            }
        }
        public decimal Balance { get; private set; }
        private Timer LoggingTimer = new Timer();
        private Timer TransactionsTimer = new Timer();

        private Parking()
        {
            ParkingPlaces = Settings.ParkingPlaces;
            ConfigureTimers();
            LoggingTimer.Start();
           
        }
        private void ConfigureTimers()
        {
            LoggingTimer.Interval = Settings.LoggingInterval;
            TransactionsTimer.Interval = Settings.PaymentInterval;
            LoggingTimer.Elapsed += LoggingTimerOnTick;
            TransactionsTimer.Elapsed += TransactionTimerOnTick;
            

        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicles.Count < ParkingPlaces)
            {
                vehicles.Add(vehicle);
                TransactionsTimer.Start();
            }
            else
                throw new ParkingFullException("Parking full! Try later...");
        }
        public void RemoveVehicle(Guid Id)
        {
            if (vehicles.Find(v => v.Id == Id).Balance < 0)
            {
                throw new RemoveVehicleWithFineException("Vehicle has fine! Top up the balance to remove a vehicle...");
            }
            if (!vehicles.Exists(v => v.Id == Id))
            {
                throw new Exception("Vehicle with this Id not founded...");
            }
            else
            {
                vehicles.Remove(vehicles.Find(v => v.Id == Id));
            }
        }
        public decimal TopUpVehicleBalance(Guid Id, decimal amount)
        {
            if (!vehicles.Exists(v => v.Id == Id))
            {
                throw new Exception("Vehicle with this Id not founded...");
            }
            else
            {
               vehicles.Find(v => v.Id == Id).IncreaseBalance(amount);
                return vehicles.Find(v => v.Id == Id).Balance;
            }

        }
        public string ShowAllVehicles()
        {
            if (vehicles.Count != 0)
            {
                string result = String.Empty;
                foreach (var v in vehicles)
                {
                    result += v.ToString() + "\n";
                }
                return result;
            }
            return "Parking is empty...";
        }
        public string ShowDetails(Guid Id)
        {
            if(!vehicles.Exists(v => v.Id == Id))
            {
                throw new Exception("Vehicle with this Id not founded...");
            }
            else
            {
                return vehicles.Find(v => v.Id == Id).ToString();
            }
        }
        private void TransactionTimerOnTick(Object source, ElapsedEventArgs e)
        {
            foreach (Vehicle v in vehicles)
            {
                double ticks;
                decimal price = Settings.PricesDictionary[v.Type];
                if (v.Balance < price)
                {
                    price *= (decimal)Settings.FineRatio;
                }
                v.DecreaseBalance(price);
                Balance += price;
                ticks = (double)(v.Balance / price);
                if (ticks < 0) ticks = 0;
                transactions.Add(new Transaction(v.Id, price, ticks));
                transactions.RemoveAll(transaction => transaction.Time < DateTime.Now.AddMinutes(-1));
            }
        }

        private async void LoggingTimerOnTick(Object source, ElapsedEventArgs e)
        {
            using (StreamWriter file = new StreamWriter(Settings.LoggingFilePath, true))
            {
                await file.WriteLineAsync("Time: " + DateTime.Now + "; Earned for the last minute: " + BalanceForLastMinute);
                file.Flush();
            } 
        }
    }
}
