﻿using System;

namespace ParkingLot
{
    class Program
    {
        static void Main(string[] args)
        {
            IMenu menu = new MainMenu();
            menu.Run();
        }
    }
}
