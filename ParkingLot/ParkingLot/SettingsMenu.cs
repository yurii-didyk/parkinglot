﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot
{
    class SettingsMenu : IMenu
    {
        private bool flag = true;
        private readonly string menu = "\nPlease, choose action which you want to run...\n1. Show all parametrs\n2. Change configuration\n3.Exit";
        private readonly string submenu = "\n1. Change count of parking places\n2. Change fine ratio\n3. Change payment interval\n4. Change logging interval";
        public void ShowMenu()
        {
            Console.WriteLine(menu);
        }
        public void Action()
        {
            ushort choice;
            ShowMenu();
            UInt16.TryParse(Console.ReadLine(), out choice);
            Console.Clear();
            switch(choice)
            {
                case 1:
                    ShowConfiguration();
                    Timeout();
                    break;
                case 2:
                    Configurate();
                    break;
                case 3:
                    flag = false;
                    Console.WriteLine("Going to main menu...");
                    break;
                default:
                    throw new UnknownCommandException("Please input valid number...");
                    

            }
            
        }
        public void Run()
        {
            while (flag)
            {
                try
                {
                    Action();
                }
                catch (UnknownCommandException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
            
        private void ShowConfiguration()
        {
            Console.WriteLine($"Starter balance: {Settings.StarterBalance}");
            Console.WriteLine($"Parking places: {Settings.ParkingPlaces}");
            Console.WriteLine($"Fine ratio: {Settings.FineRatio}");
            Console.WriteLine($"Payment interval: {Settings.PaymentInterval} miliseconds");
            Console.WriteLine($"Logging interval: {Settings.LoggingInterval} miliseconds");
            Console.WriteLine($"Logging path: {Settings.LoggingFilePath}");
            Console.WriteLine($"Dictionary with types of vehicles and prices: ");
            foreach (KeyValuePair<VehicleType, decimal> item in Settings.PricesDictionary)
            {
                Console.WriteLine($"Vehicle type: {item.Key}, Price: {item.Value} for 1 tick");
            }
        }
        private void Configurate()
        {
            ushort choice;
            Console.WriteLine(submenu);
            UInt16.TryParse(Console.ReadLine(), out choice);
            Console.Clear();
            switch(choice)
            {
                case 1:
                    ChangeParkingPlacesCount();
                    break;
                case 2:
                    ChangeFineRatio();
                    break;
                case 3:
                    ChangePaymentInterval();
                    break;
                case 4:
                    ChangeLoggingInterval();
                    break;
                default:
                    throw new UnknownCommandException("Please input valid number...");
            }
            Timeout();
        }
        private void Timeout()
        {
            Console.ReadKey();
            Console.Clear();
        }
        private void ChangeParkingPlacesCount()
        {
            Console.WriteLine("Input new size of parking: ");
            uint count;
            bool valid = UInt32.TryParse(Console.ReadLine(), out count);
            if (!valid)
            {
                throw new FormatException("Input integer number");
            }
            Settings.ParkingPlaces = count;
            Console.WriteLine($"Success: now parking has {Settings.ParkingPlaces} parking places");
        }
        private void ChangeFineRatio()
        {
            Console.WriteLine("Input new fine ratio: ");
            float ratio;
            bool valid = Single.TryParse(Console.ReadLine(), out ratio);
            if (!valid)
            {
                throw new FormatException("Input correct number(maybe you mixed up ',' with '.'?)");
            }
            Settings.FineRatio = ratio;
            Console.WriteLine($"Success: fine ratio is {Settings.FineRatio}");
        }
        private void ChangePaymentInterval()
        {
            Console.WriteLine("Input new payment interval(in miliseconds): ");
            uint interval;
            bool valid = UInt32.TryParse(Console.ReadLine(), out interval);
            if (!valid)
            {
                throw new FormatException("Input correct number");
            }
            Settings.PaymentInterval = interval;
            Console.WriteLine($"Success: payment interval is {Settings.PaymentInterval}");
        }
        private void ChangeLoggingInterval()
        {
            Console.WriteLine("Input new logging interval(in miliseconds): ");
            uint interval;
            bool valid = UInt32.TryParse(Console.ReadLine(), out interval);
            if (!valid)
            {
                throw new FormatException("Input correct number");
            }
            Settings.LoggingInterval = interval;
            Console.WriteLine($"Success: logging interval is {Settings.PaymentInterval}");
        }
    }
}
