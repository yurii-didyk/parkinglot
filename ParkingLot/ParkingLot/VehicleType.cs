﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingLot
{
    enum VehicleType
    {
        Car,
        Bus,
        Truck,
        Motorcycle
    }
}
